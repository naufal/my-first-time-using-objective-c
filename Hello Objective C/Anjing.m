//
//  Anjing.m
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Anjing.h"

@implementation Anjing : HewanPeliharaan

- (void)bergerak {
    printf("%s%d %s\n", "Anjing bergerak dengan ke", self.jumlahKaki, "kakinya.");
}

- (void)bunyi {
    printf("%s\n", "Anjing menggonggong.");
}

@end
