//
//  HelloClass.m
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelloClass.h"

@implementation HelloClass

- (instancetype)firstName:(NSString *)firstName secondName:(NSString *)secondName {
    self.firstName = firstName;
    self.secondName = secondName;
    self.number = (int)firstName.length + (int)secondName.length;
    return self;
}

- (void)sayWelcome {
    printf("Hello, %s %s\n", self.firstName.UTF8String, self.secondName.UTF8String);
}

- (NSString*)welcomeText {
    return [NSString stringWithFormat:@"Hello, %@ %@", self.firstName, self.secondName];
}

- (int)luckyNumber {
    return self.number * 10;
}

@end
