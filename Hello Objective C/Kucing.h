//
//  Kucing.h
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#ifndef Kucing_h
#define Kucing_h

#import "HewanPeliharaan.h"

@interface Kucing : HewanPeliharaan

@end

#endif /* Kucing_h */
