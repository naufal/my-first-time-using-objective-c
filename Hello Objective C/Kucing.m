//
//  Kucing.m
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Kucing.h"

@implementation Kucing : HewanPeliharaan

- (void)bergerak {
    printf("%s%d %s\n", "Kucing bergerak dengan ke", self.jumlahKaki, "kakinya.");
}

- (void)bunyi {
    printf("%s\n", "Kucing mengeong.");
}

@end
