//
//  HewanPeliharaan.m
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HewanPeliharaan.h"

@implementation HewanPeliharaan : NSObject

@synthesize buas, jumlahKaki;

- (instancetype)init {
    self.buas = FALSE;
    self.jumlahKaki = 4;
    return self;
}

- (void)bergerak{}
- (void)bunyi{}

@end
