//
//  HewanPeliharaan.h
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#ifndef HewanPeliharaan_h
#define HewanPeliharaan_h

#import "Hewan.h"

@interface HewanPeliharaan : NSObject<Hewan>

- (void)bergerak;
- (void)bunyi;

@end

#endif /* HewanPeliharaan_h */
