//
//  main.m
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HelloClass.h"
#import "Kucing.h"
#import "Anjing.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *a = @"Hello";
        NSString *b = @"Objective";
        NSString *c = @"C";
        a = @"uuu gao~";
        printf("%s %s %s\n", a.UTF8String, b.UTF8String, c.UTF8String);
        HelloClass* hello = [[HelloClass alloc] firstName:@"Naufal" secondName:@"Fachrian"];
        printf("%s\n", hello.welcomeText.UTF8String);
        [hello sayWelcome];
        printf("%d\n", hello.luckyNumber);
        Kucing *kucing = [[Kucing alloc] init];
        [kucing bergerak];
        [kucing bunyi];
        Anjing *anjing = [[Anjing alloc] init];
        [anjing bergerak];
        [anjing bunyi];
    }
    return 0;
}
