//
//  HelloClass.h
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#ifndef HelloClass_h
#define HelloClass_h

@interface HelloClass : NSObject

@property NSString *firstName;

@property NSString *secondName;

@property int number;

- (instancetype)firstName:(NSString*)firstName secondName:(NSString*)secondName;

- (void) sayWelcome;

- (NSString*) welcomeText;

- (int) luckyNumber;

@end

#endif /* HelloClass_h */
