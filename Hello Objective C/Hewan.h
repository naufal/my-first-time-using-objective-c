//
//  Hewan.h
//  Hello Objective C
//
//  Created by Naufal Fachrian on 5/31/17.
//  Copyright © 2017 Naufal Fachrian. All rights reserved.
//

#ifndef Hewan_h
#define Hewan_h

@protocol Hewan

- (void) bunyi;

- (void) bergerak;

@property int jumlahKaki;

@property BOOL buas;

@end

#endif /* Hewan_h */
